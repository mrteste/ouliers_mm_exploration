# Guide de démarrage

Pour utiliser un système de gestion de version comme gitlab, il faut utiliser deux outils additionnels :   

- le site gitlab.com qui héberge le projet  
- l'outil git bash sur Windows qui servira à interagir avec le projet  


## Préparation

- s'inscrire sur gitlab.com  
- installer git pour windows (git bash)  
- ouvrir git bash  
- taper la commande suivante  `ssh-keygen -t ed25519 -C "macle"`

gitbash vous demandera d'entrer un mot de passe : pas besoin, taper deux fois sur entrée

- récupérer votre clé en tapant la commande dans git bash `cat ~/.ssh/id_ed25519.pub | clip`  

Cette commande vous permet de copier la clé dans le presse-papier

- connectez-vous sur [www.gitlab.com](www.gitlab.com) avec identifiant et mot de passe, 
- dans votre profil, aller dans settings -> **SSH Keys**
- coller votre clé SSH dans l'endroit indiqué
- taper la commande suivante dans git bash `cd /x/xx`   

où x est le disque et xx le chemin sur disque vers le répertoire qui contiendra le dossier du projet
Cette commande ne fait que changer le répertoire de travail (cd = change directory)

## Pour clôner le projet sur son poste 

Clôner un projet, c'est récupérer l'ensemble du répertoire en ligne pour le déposer à l'identique sur son poste de travail. Pour cela, dans git bash, taper la commande suivante :

```
git clone https://gitlab.com/mrteste/pisa_multi_ensai.git
```

La commande créera le dossier packrologit-ensai dans le dossier de travail que vous avez mentionné avant avec la commande cd
Il est possible que Windows vous demande vos identifiants à gitlab, tapez les et faites entrer.

Le dossier devrait être clôné dans votre répertoire de travail.

## Documentation sur git 

[Un lien vers une documentation complète sur git ](https://git-scm.com/doc )  

[Un lien vers les commandes essentielles de git](https://git-scm.com/book/fr/v1/Démarrage-rapide)

### Les fonctions les plus importantes

  
  
| Commande  |    Description         |      Remarques           |  
| :------------------ | :-----------------------: | :------------------------- |  
| git add monfichier.R -v    | Ajoute un fichier à la liste des fichiers suivis par git  |  Un fichier ne sera enregistré dans l'historique et ne pourra être reversé que s'il est d'abord ajouté à la liste de suivi|
| git commit -m 'message décrivant ce que j'ai mis à jour' | commande qui enregistre l'ensemble des modifications réalisées sur les fichiers de la liste de suivi             |   Attention cette enregistrement n'est que local |
| git push  | permet d'envoyer l'ensemble des modifs sur le serveur distant          |    Faire régulièrement des commit mais des push que quand c'est nécessaire | 
| git pull  | récupère en local les fichiers du serveur distant s'ils ont été modifiés |   |


### Bonnes pratiques

1. Avant de commencer à travailler : faire un `git pull` pour être à jour des modifs présentes sur le serveur
2. Au cours d'une séance de travail : `git add monfichier.R` et `git commit -m 'message de ce que j'ai fait` permettent de générer des traces dans l'historique du projet. ATTENTION : ne pas ajouter tous les fichiers au suivi par git. Tout ce qui est suivi sera ajouté sur le serveur. Mais on peut très bien créer en local des fichiers qui n'ont pas vocation à être disponibles pour tous (ex. des brouillons).
3. A la fin d'une période/séance de travail : `git log` afin de voir si tous les fichiers qu'on souhaite transférer ont bien été "committés", `git pull` pour récupérer les dernières avancées et enfin `git push` pour envoyer les dernières versions de nos fichiers sur le serveur.

Ce document omet évidemment tous les éventuels conflits de version, gérés au cas par cas. 







