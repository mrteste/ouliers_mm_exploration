##' Fonction de Test de Langford et Lewis
##'
##' cette fonction teste si un groupe prédefini est aberrant au sens de Langford et Lewis
##'
##' @param model0 objet de type lmer
##' @param groupe_suspect numéro du groupe suspecté aberrant
##' @param nom_variable_groupe nom de la variable de groupe
##'
##' @examples
##' Y<-rnorm(1000)
##' groupe<-
##' objet_lmer<-lmer()
##' objet_test<- test_LL(objet_lmer, groupe_suspect=8, nom_variable_groupe=groupe)
##' 
##' @return instance objet de type htest
##' @export
#'
# fonction de test LL


#Trois paramÃ¨tres en entrÃ©e :
# model0 : modÃ¨le obtenu avec lmer
# groupe_suspect : groupe dont on veut tester l'aberrance avec le test de LL
# nom_variable_groupe : nom de la variable segmentant les donnÃ©es en groupes
test_LL<- function(model0, groupe_suspect){
  #options a ajouter eventuellement type_test = "all", out="test", modelH1 = "1d", ML = TRUE, avert = FALSE
  #recuperation des donnees
  model0<-update(model0,REML=FALSE)
  donnees <- model0@frame
  nom_variable_groupe<-names(model0@flist)
  #creation de l indicatrice de non appartenance au groupe suspect
  donnees$I_groupe <- ifelse(donnees[[nom_variable_groupe]] == groupe_suspect, 1, 0)
  #creation de la nouvelle formule de regression par ajout de l'indicatrice de non appartenance au groupe suspect
  new_formula <- update(model0@call$formula, ~. + I_groupe)
  #creation du modele alternatif
  model1 <- lmer(new_formula, data = donnees, REML=FALSE)
  #test de LL :
  #H0 : model0 vs H1: model1
  stat_TestLL <- as.numeric(-2*(logLik(model0) - logLik(model1)))
  pvaleur <- 1-pchisq(stat_TestLL, df = 1)
  #Resultats
  resultats <- list(stat_TestLL=stat_TestLL, pvaleur=pvaleur, formuleH0=model0@call$formula,
                    formuleH1=model1@call$formula, num_groupe_aberrant=groupe_suspect, ddl=1)
  instance <- list()
  instance$model1<-model1
  instance$model0<-model0
  instance$num_groupe_aberrant<-resultats$num_groupe_aberrant
  instance$nb_groupes<- ngrps(model0)
  instance$nb_obs<-nobs(model0)
  instance$formuleH0<-resultats$formuleH0
  instance$formuleH1<-resultats$formuleH1
  # null.value : numeric vector containing the value(s) of the population parameter(s) specified by the null hypothesis
  instance$null.value <- resultats$num_groupe_aberrant
  names(instance$null.value)<- "if the outlier group "
  # alternative : character string indicating the alternative hypothesis
  instance$alternative <- "two-sided"
  # method : character string giving the name of the test used.
  instance$method <-"Test Langford et Lewis"
  # estimate : numeric vector containing the value(s) of the estimated population parameter(s) involved in the null hypothesis.
  instance$estimate <- list(logLik(model0),BIC(model0),logLik(model1), BIC(model1))
  names(instance$estimate)<- list("Loglikelihood of the model under the nulle hypothesis", "BIC criterium of the model under the nulle hypothesis","Loglikelihood of the model under the alternative hypothesis", "BIC criterium of the model under the alternative hypothesis")
  # data.name : character string containing the actual name(s) of the input data.
  summ<-summary(model0)
  summcall<-summ$call
  instance$data.name<- summcall$data
  # statistic : numeric scalar containing the value of the test statistic
  instance$statistic<- resultats$stat_TestLL
  names(instance$statistic)<-"LL-statistic"
  # numeric vector containing the parameter(s) associated with the null distribution of the test statistic
  instance$parameters <-resultats$ddl
  names(instance$parameters)<-"df"
  # données sous H0 et H1
  instance$dataH0<-model0@frame
  instance$dataH1<-model1@frame
  # numeric scalar containing the p-value for the test under the null hypothesis. 
  instance$p.value<-resultats$pvaleur
  # mise en forme classe
  class(instance) <- "htest"
  return(instance)
}



