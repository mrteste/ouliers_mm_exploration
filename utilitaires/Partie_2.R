source(file=  "C:/Users/Birot-Rey/Desktop/Projet stat/func_random_intercept.R")
source(file = "C:/Users/Birot-Rey/Desktop/Projet stat/fonction_test_LL.R")
library(tidyverse)

# Premier cas : pas de groupe aberrant

## G�n�ration des donn�es

set.seed(1234)
params <- set_params(h=10, nh=100, h_out = 1, p = 0, tau0 = 3, gammas = 10, U0_out = 0, sig = 1)
data_gen <- gen_Y(params)
library(lme4)
model0 <- lmer(Y~1 + (1|groupe), data = data_gen, REML=FALSE)

### LL � la main avec le groupe 3
donnees <- model0@frame
nom_variable_groupe<-names(model0@flist)
#creation de l indicatrice de non appartenance au groupe suspect
donnees$I_groupe <- ifelse(donnees[[nom_variable_groupe]] == 3, 1, 0)
#creation de la nouvelle formule de regression par ajout de l'indicatrice de non appartenance au groupe suspect
new_formula <- update(model0@call$formula, ~. + I_groupe)
#creation du modele alternatif
model1 <- lmer(new_formula, data = donnees, REML=FALSE)
stat_TestLL <- as.numeric(-2*(logLik(model0) - logLik(model1)))
logLik(model0)
logLik(model1)

### LL � la main avec le groupe 4
donnees <- model0@frame
nom_variable_groupe<-names(model0@flist)
#creation de l indicatrice de non appartenance au groupe suspect
donnees$I_groupe <- ifelse(donnees[[nom_variable_groupe]] == 4, 1, 0)
#creation de la nouvelle formule de regression par ajout de l'indicatrice de non appartenance au groupe suspect
new_formula <- update(model0@call$formula, ~. + I_groupe)
#creation du modele alternatif
model1 <- lmer(new_formula, data = donnees, REML=FALSE)
stat_TestLL <- as.numeric(-2*(logLik(model0) - logLik(model1)))
logLik(model0)
logLik(model1)



## Utilisation du test de LL

#On utilise notre test LL sur le groupe 1.
test_LL(model0, 1)


#La statistique de test est de 0.91961 (inf�rieure � 3.84). On accepte H0. Le groupe 1 n'est pas aberrant selon le test de LL.  
#On peut refaire ce test sur l'ensemble des groupes afin de voir ce que nous donne le test de LL.

statsTest <- data.frame(matrix(NA,ncol = 4, nrow = 10))
colnames(statsTest) <- c("groupe","statsLL","pvalue","")
for (i in (1:10)){
  a <- test_LL(model0, i)
  statsTest[i,1] <- i
  statsTest[i,2] <- a[["statistic"]][["LL-statistic"]]
  statsTest[i,3] <- a[["p.value"]]
  if (a[["statistic"]][["LL-statistic"]] > 3.84) {statsTest[i,4] <- "Groupe aberrant"}
  else {statsTest[i,4] <- " "}
}
statsTest


#Le groupe 4 est pr�sum� aberrant selon le test de LL. N�anmoins, dans notre g�n�ration des donn�es, nous n'avions pas int�gr� de groupe aberrant. Le groupe 4 est donc un faux positif.  

#On peut observer la distribution des diff�r�nts groupes gr�ce � des box-plots.


donnees <- data.frame(groupe = as.factor(data_gen$groupe), Y = data_gen$Y)
ggplot(donnees) +
  geom_boxplot(aes(x = groupe, y = Y, fill = as.factor(groupe %in% c(3,4)))) +
  scale_color_brewer(palette="Dark2") +
  #theme(legend.title = element_blank()) +
  #scale_fill_discrete(labels = c("", "Faux positifs")) +
  theme(legend.position = "none") +
  geom_hline(yintercept=10, color = "red")

#La distribution du groupe 4 le place graphiquement un peu � part des autres. L'al�a de la g�n�ration de nos donn�es est responsable de cela.

test_LL(model0, 3)
test_LL(model0, 4)



# Deuxi�me cas : un groupe (fortement) aberrant

set.seed(1234)
params <- set_params(h=10, nh=100, h_out = 1, p = 0, tau0 = 3, gammas = 10, U0_out = 15, sig = 1)
data_gen <- gen_Y(params)
library(lme4)
model0 <- lmer(Y~1 + (1|groupe), data = data_gen, REML=FALSE)

donnees <- data.frame(groupe = as.factor(data_gen$groupe), Y = data_gen$Y)
ggplot(donnees) +
  geom_boxplot(aes(x = groupe, y = Y, fill = as.factor(groupe %in% c(10)))) +
  scale_color_brewer(palette="Dark2") +
  #theme(legend.title = element_blank()) +
  #scale_fill_discrete(labels = c("", "Faux positifs")) +
  theme(legend.position = "none") +
  geom_hline(yintercept=10, color = "red")

## Utilisation du test de LL

#On utilise notre test LL sur le groupe 10.
test_LL(model0, 10)

### LL � la main avec le groupe 10
donnees <- model0@frame
nom_variable_groupe<-names(model0@flist)
#creation de l indicatrice de non appartenance au groupe suspect
donnees$I_groupe <- ifelse(donnees[[nom_variable_groupe]] == 10, 1, 0)
#creation de la nouvelle formule de regression par ajout de l'indicatrice de non appartenance au groupe suspect
new_formula <- update(model0@call$formula, ~. + I_groupe)
#creation du modele alternatif
model1 <- lmer(new_formula, data = donnees, REML=FALSE)
stat_TestLL <- as.numeric(-2*(logLik(model0) - logLik(model1)))
logLik(model0)
logLik(model1)
