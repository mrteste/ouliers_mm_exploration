---
title: "Test du niveau de risque du test LL"
author: "Nicolas Birot, Olivier Criquet et Julien Jamme"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output: 
  pdf_document:
    keep_tex: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r warning=FALSE, message=FALSE}
packages <- c("dplyr","tidyr","purrr","tibble","forcats",
              "ggplot2","magrittr","lme4","parallel")
p<-lapply(packages, function(x) suppressPackageStartupMessages(library(x, character.only = TRUE)))
# getwd()
# chemin_repertoire <- "~/outliers_mm_exploration"
# setwd(chemin_repertoire)
source(file = "func_random_intercept.R", encoding = "UTF8")
```

Notations :

- k : numéro du groupe testé,
- $\alpha$ : paramètre testé - erreur de type I du test de Langford Lewis,
- $\alpha_0$ : valeur du paramètre sous $H_0$,
- $\hat\alpha$ : estimateur du paramètre, c'est-à-dire proportion de cas où le test de LL conduit à rejeter son hypothèse nulle,
- n : taille de l'échantillon utilisé pour l'estimation de $\hat\alpha$,
- $T_1, ..., T_n$ n-échantillon iid de loi $\mathcal{B}(\alpha)$. Chaque variable aléatoire est le résultat du test de LL mené sur le groupe k, prenant la valeur 1 en cas de succès (rejet de l'hypothèse nulle du testde LL), 0 sinon. On a la relation : $\hat\alpha = \frac{1}{n}\sum_{i=1}^{n}{T_i}$.

On mène ainsi un test sur la proportion $\alpha$ de rejets de l'hypothèse nulle du test de LL sur le groupe k.

Structure du test :

- Hypothèses : $H_0 : \alpha =  \alpha_0$ vs $H_1 : \alpha \not = \alpha_0$  
- Statistique de test : $\hat \alpha$
- Sous $H_0, n\hat \alpha \sim \mathcal{B}(n, \alpha_0)$

Or, $\hat\alpha = \frac{1}{n}\sum_{i=1}^{n}{T_i}$, où les $T_i$ sont des variables aléatoires indépendantes et identiquement distribuées de moyenne $\alpha$ et de variance $\alpha(1-\alpha)$. Donc par le théorème central limite et sous $H_0$, 

$$\sqrt{n} \frac{\hat\alpha - \alpha_0}{\sqrt{\alpha_0(1-\alpha_0)}} \Rightarrow \mathcal{N}(0,1)$$

On considère\footnote{Voir M. Lejeune, *Statistique : La théorie et ses applications*, Springer, 2010, p. 85} que l'approximation est convenable avec $n\alpha\geq5$ et $n(1-\alpha)\geq5$, c'est-à-dire avec $n \geq 100$ quand $\alpha = 0.05$. Or, nous verrons plus loin que nous sommes amenés à construire des échantillons de beaucoup plus grande taille.  

- Zone de rejet du test pour un niveau de risque $\alpha^\prime$: $\mathcal{R} = \{ \hat\alpha : \sqrt{n}\big|\frac{\hat\alpha - \alpha_0}{\sqrt{\alpha_0(1-\alpha_0)}}\big| \geq z_{1-\alpha^\prime/2}\}$, où $z_{1-\alpha^\prime/2}$ est le quantile d'ordre $1-\alpha^\prime/2$ de la loi $\mathcal{N}(0,1)$.

- On peut également calculer l'intervalle de confiance de niveau de confiance $1-\alpha^\prime$ :

$$ IC_{1-\alpha^\prime} = \huge{[} \small \hat\alpha {+/-}  z_{1-\alpha^\prime/2} \sqrt{\frac{\alpha(1-\alpha)}{n}}  \huge{]}$$

Cet intervalle de confiance nous permet de calculer la taille minimale de l'échantillon à construire pour obtenir une certaine précision de l'estimation de $\alpha$ sous $H_0$. On note $\delta$, la précision absolue recherchée dans l'estimation de $\alpha$.

\begin{align}
2\delta \geq 2 z_{1-\alpha^\prime/2} \sqrt{\frac{\alpha_0(1-\alpha_0)}{n}} 
&\iff \frac{\delta}{z_{1-\alpha^\prime/2}} \geq \sqrt{\frac{\alpha_0(1-\alpha_0)}{n}}\\
&\iff (\frac{\delta}{z_{1-\alpha^\prime/2}})^2 \geq \frac{\alpha_0(1-\alpha_0)}{n}\\
&\iff n \geq \frac{\alpha_0(1-\alpha_0)z_{1-\alpha^\prime/2}^2}{\delta^2}\\
\end{align}

Application numérique : 

```{r}
taille_ech_fun <- function(a0, risq, prec) ceiling(a0*(1-a0)*qnorm(1-risq/2)^2/prec^2)
vtaille_ech_fun <- Vectorize(taille_ech_fun)
risq <- rep(0.05,3)
a0 <- c(0.01, 0.05, 0.1)
prec <- rep(10^(-3),3)
tailles_ech <- data.frame(a0=a0,risq=risq,prec=prec)
tailles_ech$n <- vtaille_ech_fun(tailles_ech$a0, tailles_ech$risq, tailles_ech$prec)
```


```{r, echo=FALSE}
knitr::kable(
  tailles_ech, 
  caption = "Taille minimale d'un échantillon selon le niveau du test de LL souhaité (a0) pour une précision de l'estimation à 10^-3 près et un intervalle de confiance de 95%")
```


Comme, en pratique $\alpha$ n'est pas connu, on utilisera l'estimateur d'intervalle de confiance suivant :

$$ \hat{IC}_{1-\alpha^\prime} = \huge{[} \small \hat\alpha {+/-}  z_{1-\alpha^\prime/2} \sqrt{\frac{\hat\alpha(1-\hat\alpha)}{n}}  \huge{]}$$




## Simulation

### Algorithme :

Pour i allant de 1 à n, faire :

  - générer des données aléatoirement
  - réaliser le test de LL sur un groupe aléatoirement tiré avec pour niveau de risque $\alpha_0$
  - si le test conduit à rejeter l'hypothèse nulle, $t_i=1$ sinon $t_i=0$

Fin

Une fois l'ensemble des $t_i$ obtenus, on peut calculer notre estimateur $\hat\alpha$, la zone de rejet et l'intervalle de confiance à 95%.



```{r}
f_test_one_gr <- function(params, num_groupe, alpha = 0.05){
  require(dplyr)
  require(lme4)
  data <- gen_Y(params)
  model0 <- lmer(Y~1 + (1|groupe), data = data, REML = FALSE)
  data$I<- data$groupe == num_groupe
  model1d <- lmer(Y~1 + I + (1 | groupe), data = data, REML = FALSE)
  stat_testLL <- -2*(logLik(model0) - logLik(model1d))
  pvaleur <- 1-pchisq(stat_testLL, df = 1)
  return(pvaleur < alpha)
}
```

### Les plans

On souhaite obtenir une estimation du risque de première espèce dans différents cas, en fonction du nombre de groupes et du ratio $\tau_0/\sigma$, racine carrée du ratio de la variabilité inter-groupe sur la variabilité intra-groupe.

On fixe un nombre d'individus identique par groupe et l'écart-type du terme d'erreur individuel :

```{r}
nbindbygroupe = 20
sigma = 1
```

```{r}
plans <- tibble(
  nbgroupes = c(5,10,20,100,100,20,10),
  factmult = c(0.01,0.1,0.5,1,2,10,100)
) %>% 
  tidyr::expand(nbgroupes,factmult) %>%
  rowid_to_column(var = "no_plan")
```


### Résultats pour alpha = 0.05

On arrondit la taille minimale de l'échantillon à 5000 près supérieurement.

```{r}
alpha0 <- 0.05 #niveau de risque du test de LL
n_min = tailles_ech$n[tailles_ech$a0==alpha0]
nb_simulations = n_min - n_min%%5000 + 5000
alphaprime <- 0.05 # niveau de risque du test mené sur les résultats de la simulation
```


```{r eval=FALSE, include=TRUE}
no_cores <- detectCores() - 1
cat("nombre de coeurs sollicités", no_cores)
nbsimbycores <- c(rep(trunc(nb_simulations/no_cores),no_cores-1),
                  trunc(nb_simulations/no_cores)+nb_simulations%%no_cores)
res_simul <- numeric(length=nrow(plans))
start <- proc.time()
for(plan in transpose(plans)){
  cl <- makeCluster(no_cores)
  clusterExport(
    cl,
    c("f_test_one_gr", "set_params", "gen_Y",
      "gen_expl", "gen_alea", "nb_simulations", "alpha0",
      "nbindbygroupe", "sigma", "plan", "nbsimbycores", "res_simul")
  )
  res_simul[plan$no_plan] <- sum(parSapply(cl, nbsimbycores, function(x){
    params <- set_params(h=plan$nbgroupes, nh=nbindbygroupe,
                         h_out = 0, p = 0, tau0 = sigma*plan$factmult,
                         gammas = 10, U0_out = 0, sig = sigma)
    return(
      sum(replicate(x,
          suppressWarnings(suppressMessages(
            f_test_one_gr(params=params,
                          num_groupe=sample(1:plan$nbgroupes,1),
                          alpha=alpha0))))
      )
    )
  }))
  stopCluster(cl)
  ecoule <- proc.time() - start
}
saveRDS(list(no_cores,ecoule,res_simul), file="estim_err_typI_5pc.rds")
```


```{r}
res_simul1 <- readRDS("estim_err_typI_5pc.rds")[[3]]
df_res_sim1 <- plans
df_res_sim1$nb_rejets <- res_simul1
df_res_sim1$alpha_hat <- res_simul1/nb_simulations
df_res_sim1$test <- lapply(df_res_sim1$nb_rejets, 
                           prop.test, n=nb_simulations, p=alpha0,
                           alternative="two.sided", conf.level=1-alphaprime,
                           correct=FALSE)
df_res_sim1$pvaleur <- sapply(df_res_sim1$test, function(x) x$p.value)
df_res_sim1$rejet <- sapply(df_res_sim1$test, function(x) x$p.value < alphaprime)
comp_conf_int <- function(est) qnorm(0.975)*sqrt(est*(1-est)/nb_simulations)
df_res_sim1$confint_min <- df_res_sim1$alpha_hat - comp_conf_int(df_res_sim1$alpha_hat)
df_res_sim1$confint_max <- df_res_sim1$alpha_hat + comp_conf_int(df_res_sim1$alpha_hat)
```

```{r, echo=FALSE}
df_res_sim1 %>% 
  select(nbgroupes,factmult,alpha_hat,pvaleur, rejet, confint_min,confint_max) %>%
  knitr::kable(caption = "Résultat du test et intervalle de confiance à 95% de l'erreur de type I du test de LL (Hypothèses H0: alpha = 0.05 vs H1: alpha ne 0.05)")
```

<!--Nous avons, par exemple, regarder le cas du jeu de données sleepstudy qui accompagne le package lme4. C'est un ensemble de mesures du temps de réaction d'individus selon le nombre de jours de privation de sommeil. La variable de groupe (Subject) compte 18 modalités. L'estimation par lmer conduit à un ratio tau0/sigma autour de 1 (cf figure ci-dessous). Dans ce cas, si nous regardons, notre tableau (20 groupes et facteur multiplicateur de 1), le risque du test est plus de l'ordre de 6.7% que 5%.
%Dans l'article de Langford et Lewis, les auteurs étudient un modèle à deux niveaux, certes à effets aléatoires portant sur l'intercept et sur la pente. Les estimations du ratio tau0/sigma est plutôt de l'ordre de 3.10^-2 (1/37 selon le tableau 5, p.138 de leur article). Avec 18 groupes dans leurs données, nos simulations estiment le risque à 4.1%. Cette situation où la variabilité résiduelle au niveau 1 (individus) est beaucoup plus forte que la variabilité au niveau 2 (groupe) nous semble plus réaliste quand les groupes sont des écoles ou des cliniques, -->
