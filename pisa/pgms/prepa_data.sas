/*PROGRAMME PREPARANT LES TABLES DE TRAVAIL A PARTIR DES TABLES PISA PRESENTES SOUS U*/

PROC CONTENTS DATA = libdata.pisa_fr2015_sch; RUN;
PROC CONTENTS DATA = libdata.pisa_fr2015_stu; RUN;

DATA libproj.students (
    DROP = 
        ST005Q01TA ST006:
        ST007Q01TA ST008:
        ST127:
        ST011Q07TA ST011Q08TA
        /*EC001: /* les variables EC001 ne semblent pas contenir de donn�es : une seule modalit� N */
    RENAME = (
        cntschid = school_ident
        cntstuid = student_ident
        W_FSTUWT = student_weight
        ST001D01T = student_level
        wealth = parents_wealth 
        escs = household_index_socio_eco
        pared = parents_education
        homepos = household_possession
        hisei = parents_highest_ocupation
        repeat = student_repetition
        age = student_age
        outhours = student_outhours
        ST004D01T = student_gender
        ST113Q01TA = student_useful_science_1
        ST113Q02TA = student_useful_science_2
        ST113Q03TA = student_useful_science_3
        ST113Q04TA = student_useful_science_4
        ST111Q01TA = student_aimed_diploma /* Dipl�me vis� par l'�l�ve*/
        /*ST062Q01TA ST062Q02TA ST062Q03TA /*Absences ou retards non autoris�s en cours*/
        ST011Q01TA = student_desk_for_work
        ST011Q02TA = student_room_self
        ST011Q03TA = student_quiet_work
        ST011Q12TA = student_dictionary
        /*ST034: /*ressenti de l'�l�ve vis-�-vis de son �tablissement (comme � la maison ou comme un �trnger, sentiment de solitude?*/
        PA042Q01TA = household_income /*Revenu annuel du m�nage*/
        PA041Q01TA = household_spent_for_school/*Somme d�pens�e pour la scolarit� de l'enfant*/
    )
)
;
SET libdata.pisa_fr2015_stu (
    KEEP = 
        cntschid /*identifiant �cole - school's identifier*/
        cntstuid /*identifiant �l�ves - student's identifier*/
        W_FSTUWT /*pond�ration - WEIGHT for students' answers*/
        ST001D01T /*classe de l'�l�ve - school level of the student*/
        wealth /*family wealth built BY pisa from ST011 and ST012 variables*/
        escs /*index of socio economic and cultural level of the household built BY pisa from ST005 to ST008 and ST011 to ST015 variables*/
        pared /*parent education - estimated number of schooling years, built from ST005 to ST008 variables - first index from which escs is built BY pisa*/
        homepos /*home possessions - items presented at home, built from ST011 to ST013 variables  - second index from which escs is built BY pisa*/
        hisei /*highest parent occupation, built from ST014 and ST015 variables - third index from which escs is built BY pisa*/
        ST004D01T /*gender */
        ST005Q01TA /* Scolarit� secondaire de la m�re - r�ponse de l'�l�ve -- mother's schooling, student said*/
        ST006: /* Diplome de la m�re - r�ponse de l'�l�ve -- mother's diploma, student said*/
        ST007Q01TA /* Scolarit� secondaire du p�e - r�ponse de l'�l�ve -- father's schooling, student said*/
        ST008: /* Diplome du p�re - r�ponse de l'�l�ve -- father's diploma, student said*/
        ST127: /* redoublements � l'�cole primaire -- IF the student have already repeated a year before the "college"*/
        ST113: /*Importance accord�e � l'apprentissage des sciences pour leur avenir -- does the student consider that learning sciences is important for his own future */
        ST111Q01TA /* Dipl�me vis� par l'�l�ve -- diploma/level which the student wish reach/complete */
        /*ST062: Absences ou retards non autoris�s en cours - absences or delays of the student which weren't justified*/
        ST011Q01TA ST011Q02TA ST011Q03TA ST011Q07TA ST011Q08TA ST011Q12TA /*Variables sur la pr�sence � la maison d'�l�ments comme un bureau pour travailler une chambre � soi des livres classiques des dictionnaires -- At home, is there a desk WHERE the student can work, are there any classical books or dictionaries*/
        /*ST034Q01TA ST034Q02TA ST034Q03TA ST034Q04TA ST034Q05TA ST034Q06TA /*ressenti de l'�l�ve vis-�-vis de son �tablissement (comme � la maison ou comme un �tranger, sentiment de solitude? - How does the student feels himself into the school : like at home , like a stranger */
        PA042Q01TA /*Revenu annuel du m�nage -- household's annual income */
        PA041Q01TA /*Somme d�pens�e pour la scolarit� de l'enfant -- money spent BY parents only for improving student's level, except school's costs */
        /*PA037 /*Dipl�mes du p�re - r�ponse du parent r�pondant -- father's diplomas - according to the parent who answered */
        /*PA038 /*Dipl�mes de la m�re - r�ponse du parent r�pondant -- mother's diplomas - according to the parent who answered*/
        PV1READ PV2READ PV3READ PV4READ PV5READ PV6READ PV7READ PV8READ PV9READ PV10READ /*valeurs plausibles pour la compr�hension �crite -- reading understanding plausible values so that the reading level can be computed*/
        REPEAT /*Redoublement - variable binaire - grade repetition built BY Pisa from ST127 variables*/
        AGE /*Age - variable rebuilt BY pisa from ST003 variable*/
        OUTHOURS /*Number of hours spent to extra school studies, per week - built BY Pisa from ST071 variable*/
        /*EC001: /* Cours suppl�mentaire de lecture -  */        
);
LABEL
    student_diploma_level_parents = "High level reached BY one parent at least (built)"
    /*student_if_repeated = "IF the student repeated at least one year (built)" -- inutile*/
;

/*agr�gation des donn�es sur le niveau atteint par les parents en utilisant la variable sur la scolarit� secondaire et les variables sur les dipl�mes universitaires*/
/*cette variable est en r�alit� inutile puisque pisa reconstruit des variables pour disposer de cette info*/
	IF ST006Q01TA = 1 | ST006Q02TA = 1 | ST008Q01TA = 1 | ST008Q02TA = 1 THEN student_diploma_level_parents = 1;
	ELSE IF ST006Q03TA = 1 | ST008Q03TA = 1 THEN student_diploma_level_parents = 2;
	ELSE IF ST005Q01TA = 1 | ST007Q01TA = 1 THEN student_diploma_level_parents = 3;
	ELSE IF ST005Q01TA in (2,3) | ST007Q01TA in (2,3) THEN student_diploma_level_parents = 4;
	ELSE IF ST005Q01TA in (4,5) | ST007Q01TA in (4,5) THEN student_diploma_level_parents = 5;
	ELSE student_diploma_level_parents = .;

	/*Inutile car pisa a d�j� cr�� une variable REPEAT qui fait la m�me chose
	IF ST127Q01TA = 1 & ST127Q02TA = 1 & ST127Q03TA = 1 THEN student_if_repeated = 0;
	ELSE IF ST127Q01TA in (2,3) | ST127Q02TA in (2,3) | ST127Q03TA in (2,3) THEN student_if_repeated = 1;
	ELSE student_if_repeated = .;
	*/

	IF ST011Q07TA = 1 | ST011Q08TA = 1 THEN student_litt_at_home = 1;
	ELSE IF ST011Q07TA = 0 | ST011Q08TA = 0 THEN student_litt_at_home = 0;
	ELSE student_litt_at_home = .;

RUN;

DATA libproj.schools (
    KEEP = 
        CNTSCHID CLSIZE STRATIO  
        SC001Q01TA 
        /*SC010Q09TA /* responsabilit� dans l'admission des �tudiants dans l'�tablissement  */
        SC012: /* Motifs d'admission des �tudiants  */
        SC013Q01TA /* public or private school*/
        SC017: /*Manque de personnel, personnel peu qualif� - Instruction hindered BY*/
        /*SC036Q03NA /* Envoie des r�sultats aux parents */
        /*SC048Q01NA*/ 
        SC048Q02NA /*numeric - part estim�e par le principal du nombre d'�tudiants ayant des besoins particuliers*/
        SC048Q03NA /*numeric - part estim�e par le principal du nombre d'�tudiants provenant d'un milieu socio �conomique d�favoris�*/
        STRATUM /*type d'enseignement : gt, pro, agricole, coll�ge*/

    RENAME = (
        cntschid = school_ident
        CLSIZE = school_size_of_class
        STRATIO = school_ratio_stud_teach
        SC001Q01TA = school_size_of_city
        /*SC010Q09TA = school_decision_of_admission*/
        SC013Q01TA = school_status
        SC017Q01NA = school_pb_lackof_teachers
        SC017Q02NA = school_pb_unqual_teachers
        SC017Q03NA = school_pb_lackof_staff
        SC017Q04NA = school_pb_unqual_staff
        SC017Q05NA = school_pb_lackof_material
        SC017Q06NA = school_pb_inadeq_material
        SC017Q07NA = school_pb_lackof_infrastr
        SC017Q08NA = school_pb_inadeq_infrastr
        SC048Q02NA = school_estim_stu_with_needs
        SC048Q03NA = school_estim_stu_disadvanta
        SC012Q01TA = school_adm_performance
        SC012Q02TA = school_adm_recommendation 
        SC012Q03TA = school_adm_endorsement
        SC012Q04TA = school_adm_speciality
        SC012Q05TA = school_adm_former_stud
        SC012Q06TA = school_adm_geographic_area
        STRATUM = school_type

    )
);
SET libdata.pisa_fr2015_sch (DROP = SC012Q07TA);
RUN;


PROC CONTENTS DATA = libproj.students; RUN;
PROC CONTENTS DATA = libproj.schools; RUN;

PROC sort DATA = libproj.students; BY school_ident; RUN;
PROC sort DATA = libproj.schools;  BY school_ident; RUN;

DATA libproj.students_schools;
	MERGE libproj.students libproj.schools;
	BY school_ident;
RUN;

DATA libproj.public;
	SET libproj.students_schools;
	IF school_status = 1;
RUN;

DATA libproj.private;
	SET libproj.students_schools;
	IF school_status = 2;
RUN;
DATA libproj.lyceensgt;
	SET libproj.students_schools;
	IF school_type = "FRA0101";
RUN;
DATA libproj.colagripro;
	SET libproj.students_schools;
	IF school_type in ("FRA0202","FRA0203","FRA0204");
RUN;
