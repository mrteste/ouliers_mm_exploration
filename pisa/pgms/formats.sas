
PROC FORMAT;

    VALUE gender
        . = "Missing"
        1 = "Femme"
        2 = "Homme"
        9999 = "Ensemble"
    ;

    VALUE student_level
        7 = "cinqui�me"
        8 = "quatri�me"
        9 = "troisi�me"
        10 = "seconde"
        11 = "premi�re"
        12 = "terminale"
        . = "Missing"
    ;
    VALUE student_level_category
        7-8 = "cinqui�me ou quatri�me"
        9 = "troisi�me"
        10 = "seconde"
        11-12 = "premi�re ou terminale"
        . = "Missing"
    ;
    VALUE student_level_category
        7-8 = "cinqui�me ou quatri�me"
        9 = "troisi�me"
        10 = "seconde"
        11-12 = "premi�re ou terminale"
        . = "Missing"
    ;

    VALUE $school_type /*correspond � la variable initiale stratum de la TABLE schools*/
        "FRA0101" = "Lyc�e GT"
        "FRA0202" = "Coll�ge"
        "FRA0203" = "Lyc�e PRO"
        "FRA0204" = "Lyc�e agricole"
        "FRA0205" = "Autres"
        "FRA0206" = "Autres"
        "FRA9797" = "Missing"
    ;
    VALUE $school_type_bis /*correspond � la variable initiale stratum de la TABLE schools*/
        "FRA0101" = "Lyc�e GT"
        "FRA0202" = "Coll., lyc. pro ou agr."
        "FRA0203" = "Coll., lyc. pro ou agr."
        "FRA0204" = "Coll., lyc. pro ou agr."
        "FRA0205" = "Autres"
        "FRA0206" = "Autres"
        "FRA9797" = "Missing"
    ;


    VALUE student_schooling_parents
        1 = "Lyc�e g�n�ral"
        2 = "Lyc�e professionnel"
        3 = "Coll�ge"
        4 = "�cole primaire"
        5 = "Elle n'a pas fini l'�cole primaire"
        . - .M = "Missing"
    ;

    VALUE student_schooling_parents_category
        1 = "Lyc�e g�n�ral"
        2-3 = "Coll�ge ou lyc�e professionnel"
        4-5 = "�cole primaire au mieux"
        . - .M = "Missing"
    ;

    VALUE student_diploma_level_parents
        1 = "Bac +3 au moins - at least a bachelor "
        2 = "Bac +2 au plus - short cycle degree "
        3 = "Scolarit� jusqu'au lyc�e g�n�ral - general highschool"
        4 = "Scolarit� en coll�ge ou lyc�e professionnel - college or professionnal highschool"
        5 = "Scolarit� primaire au mieux - primary scholarship or less"
         . = "Missing"
    ;

    VALUE yes_no
        1 = "oui - yes"
        0 = "non - no"
        . = "Missing"
    ;

    VALUE oui_non
        1 = "oui - yes"
        2 = "non - no"
        . = "Missing"
    ;

    VALUE useful_science
        1-2 = "D'accord - agree"
        3-4 = "Pas d'accord - disagree"
        . - .M = "Missing"
    ;

    VALUE aimed_diploma
        1 = "Brevet"
        2 = "Bac pro, BEP, CAP"
        3 = "Bac"
        5 = "BTS, DUT, etc."
        6 = "Licence ou plus"
        . - .M = "Missing"
    ;

    VALUE household_income
        1 = "Moins de 15 000 euros."
        2 = "de 15 000 euros � moins de 22 500 euros"
        3 = "de 22 500 euros � moins de 30 000 euros"
        4 = "de 30 000 euros � moins de 37 500 euros"
        5 = "de 37 500 euros � moins de 45 000 euros"
        6 = "45 000 euros ou plus"
        other = "Missing"
    ;

    VALUE school_size_city
        1 = "Collectivit� de moins de 3 000 habitants"
        2 = "Bourg de 3 000 � 15 000 habitants"
        3 = "Petite ville de 15 000 � 100 000 habitants"
        4 = "Ville de 100 000 � 1 000 000 habitants"
        5 = "Agglom�ration de plus d'un million d'habitants"
    ;

    VALUE school_decision_admission
        1 = "Jamais"
        2 = "Parfois"
        3 = "Toujours"
        other = "Missing"
    ;
    VALUE school_decision_admission_bis
        1 = "Jamais ou Parfois"
        2 = "Jamais ou Parfois"
        3 = "Toujours"
        other = "Missing"
    ;

    VALUE school_public_private
        1 = "Public"
        2 = "Private"
        other = "Missing"
    ;

    VALUE school_pb
        1 = "Pas du tout"
        2 = "Tr�s peu"
        3 = "Dans une certaine"
        4 = "mesure Beaucoup"
        other = "Missing"
    ;

    VALUE lvl_read
        low-<407 = 'Id�e principale du texte'
        407-<553 = 'Relations dans le texte'
        553-<626 = 'Interpr�tation du texte'
        626-high = 'D�duction d informations � partir du texte'
    ;    

    VALUE if_repeated
        0 = 'Jamais - never'
        1 = 'Une ou plusieurs fois - one or several times'
        . = 'Missing'
    ;

RUN;

%deciles_escs(DATA = libproj.students_schools, prefixe = escs_);

DATA deciles_escs (RENAME = ( _name_ = stat _LABEL_= LABEL col1 = valeur));
    SET deciles_escs maxmin_escs;
    call symput(_name_ , col1);
RUN;

PROC FORMAT;
    VALUE escs_deciles
        9999 = 'Ensemble'
        %sysevalf(&escs_min.) - %sysevalf(&escs_p10.) = 'd1'
        %sysevalf(&escs_p10.) - %sysevalf(&escs_p20.) = 'd2'
        %sysevalf(&escs_p20.) - %sysevalf(&escs_p30.) = 'd3'
        %sysevalf(&escs_p30.) - %sysevalf(&escs_p40.) = 'd4'
        %sysevalf(&escs_p40.) - %sysevalf(&escs_p50.) = 'd5'
        %sysevalf(&escs_p50.) - %sysevalf(&escs_p60.) = 'd6'
        %sysevalf(&escs_p60.) - %sysevalf(&escs_p70.) = 'd7'
        %sysevalf(&escs_p70.) - %sysevalf(&escs_p80.) = 'd8'
        %sysevalf(&escs_p80.) - %sysevalf(&escs_p90.) = 'd9'
        %sysevalf(&escs_p90.) - %sysevalf(&escs_max.) = 'd10'
        other = 'Missing'
        ;
RUN;

%deciles_escs(DATA = libproj.lyceensgt, prefixe = escs_lgt_);
DATA deciles_escs (RENAME = (_name_ = stat _LABEL_= LABEL col1 = valeur));
    SET deciles_escs maxmin_escs;
    call symput(_name_ , col1);
RUN;
PROC FORMAT;
    VALUE escs_deciles_lyceens
        9999 = 'Ensemble'
        %sysevalf(&escs_lgt_min.) - %sysevalf(&escs_lgt_p10.) = 'd1'
        %sysevalf(&escs_lgt_p10.) - %sysevalf(&escs_lgt_p20.) = 'd2'
        %sysevalf(&escs_lgt_p20.) - %sysevalf(&escs_lgt_p30.) = 'd3'
        %sysevalf(&escs_lgt_p30.) - %sysevalf(&escs_lgt_p40.) = 'd4'
        %sysevalf(&escs_lgt_p40.) - %sysevalf(&escs_lgt_p50.) = 'd5'
        %sysevalf(&escs_lgt_p50.) - %sysevalf(&escs_lgt_p60.) = 'd6'
        %sysevalf(&escs_lgt_p60.) - %sysevalf(&escs_lgt_p70.) = 'd7'
        %sysevalf(&escs_lgt_p70.) - %sysevalf(&escs_lgt_p80.) = 'd8'
        %sysevalf(&escs_lgt_p80.) - %sysevalf(&escs_lgt_p90.) = 'd9'
        %sysevalf(&escs_lgt_p90.) - %sysevalf(&escs_lgt_max.) = 'd10'
        other = 'Missing'
        ;
RUN;

/*deciles d'escs pour les coll�giens, lyc�ens pro et agricoles*/
%deciles_escs(DATA = libproj.colagripro, prefixe = escs_colagripro_);
DATA deciles_escs (RENAME = (_name_ = stat _LABEL_= LABEL col1 = valeur));
    SET deciles_escs maxmin_escs;
    call symput(_name_ , col1);
RUN;
PROC FORMAT;
    VALUE escs_deciles_colagripro
        9999 = 'Ensemble'
        %sysevalf(&escs_colagripro_min.) - %sysevalf(&escs_colagripro_p10.) = 'd1'
        %sysevalf(&escs_colagripro_p10.) - %sysevalf(&escs_colagripro_p20.) = 'd2'
        %sysevalf(&escs_colagripro_p20.) - %sysevalf(&escs_colagripro_p30.) = 'd3'
        %sysevalf(&escs_colagripro_p30.) - %sysevalf(&escs_colagripro_p40.) = 'd4'
        %sysevalf(&escs_colagripro_p40.) - %sysevalf(&escs_colagripro_p50.) = 'd5'
        %sysevalf(&escs_colagripro_p50.) - %sysevalf(&escs_colagripro_p60.) = 'd6'
        %sysevalf(&escs_colagripro_p60.) - %sysevalf(&escs_colagripro_p70.) = 'd7'
        %sysevalf(&escs_colagripro_p70.) - %sysevalf(&escs_colagripro_p80.) = 'd8'
        %sysevalf(&escs_colagripro_p80.) - %sysevalf(&escs_colagripro_p90.) = 'd9'
        %sysevalf(&escs_colagripro_p90.) - %sysevalf(&escs_colagripro_max.) = 'd10'
        other = 'Missing'
        ;
RUN;

%quartiles_escs(DATA = libproj.students_schools);

DATA quartiles_escs (RENAME = (_name_ = stat _LABEL_= LABEL col1 = valeur));
    SET quartiles_escs;
    call symput(_name_ , col1);
RUN;

PROC FORMAT;
    VALUE escs_quartiles
        9999 = 'Ensemble'
        %sysevalf(&escs_min.) - %sysevalf(&escs_q1.) = 'q1'
        %sysevalf(&escs_q1.) - %sysevalf(&escs_q2.) = 'q2'
        %sysevalf(&escs_q2.) - %sysevalf(&escs_q3.) = 'q3'
        %sysevalf(&escs_q3.) - %sysevalf(&escs_max.) = 'q4'
        other = 'Missing'
        ;
RUN;


