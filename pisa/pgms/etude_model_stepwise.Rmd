---
title: "Analyse modèle PISA"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
setwd(dir = "P:/projet_stat/ouliers_mm_exploration")
```

## Modèle choisi par expertise

On choisi d'expliquer le niveau en lecture par l'indice socio éco, le genre et le statut de l'école. 
On effectue une régression linéaire simple : 
```{r library, include=FALSE, echo=FALSE}
library(lme4)
library(MASS)
library(lmerTest)
library(stats)
setwd(dir = "P:/projet_stat/ouliers_mm_exploration")
pisa <- readRDS("pisa/data/lyceensGT.rds") #sans pondération
pisa_ss_na<-na.omit(pisa) # on supprime ligne NA soit 240 lignes sur 2882 initialement
```

```{r model choisi lm}
notre_model_lm<-lm(students_score_lecture ~ 1 + household_index_socio_eco + school_status + student_gender,data = pisa_ss_na)
paste0("BIC = ",BIC(notre_model_lm))
paste0("AIC = ",AIC(notre_model_lm))
```


On modélise ensuite avec un modèle à effets mixtes avec effets aléatoire sur l'intercept. la variable groupe est l'établisement:

```{r model choisi lmer}
notre_model_lmer<-lmer(pisa_ss_na$students_score_lecture~ 1 + household_index_socio_eco + school_status + student_gender + (1|school_ident),data = pisa_ss_na)
paste0("BIC = ",BIC(notre_model_lmer))
paste0("AIC = ",AIC(notre_model_lmer))
```
Les baisses de l'AIC et BIC justifient bien l'utilisation d'un modèle à effets mixtes

On étudie la normalité des résidus avec test de Shapiro et qqplot: 
```{r model choisi residus}
residus<-residuals(notre_model_lmer)
shapiro.test(residus)
qqnorm(((residus-mean(residus))/sd(residus)), main="Modele lmer par expertise : QQplot des résidus")
qqline(rnorm(100,0,1), col="red")
```

 On étudie ensuite la normalité des effets aléatoires avec test de Shapiro et qqplot:
```{r model choisi ranef}
ranef<-ranef(notre_model_lmer)
ranef<-ranef[[1]]
ranef<-ranef[,1]
shapiro.test(ranef)
qqnorm(((ranef-mean(ranef))/sd(ranef)),main="Modele lmer par expertise : QQplot des effets aléatoires")
qqline(rnorm(100,0,1), col="red")
```


## Modèle par stepwise

Procédure de stepwise : 
On montre ici le modèle qui résulte de la méthode "backward/forward" mais les méthodes stepwise par "backward" et "forward" donne le même modèle.

```{r stepwise}
full.model <- lmer(students_score_lecture ~ 1 + student_level + 
                   student_gender + student_desk_for_work + student_room_self + student_quiet_work +
                   student_dictionary + student_aimed_diploma + student_repetition + student_outhours + 
                   student_litt_at_home + school_size_of_city + school_status + school_pb_lackof_teachers +                     school_pb_lackof_staff + school_pb_lackof_material + school_pb_lackof_infrastr +                             school_estim_stu_with_needs + school_estim_stu_disadvanta +                                                  school_ratio_stud_teach + household_index_socio_eco + (1|school_ident), data = pisa_ss_na)

step<-lmerTest::step(full.model)
step
```


Modélisation avec modèle avec effets mixtes sur les variables selectionnées par stepwise:
```{r stepwise summary}
step.model<-lmer(students_score_lecture ~ 1 + student_level + student_gender + student_aimed_diploma + 
    student_outhours + student_litt_at_home + school_estim_stu_disadvanta + 
    household_index_socio_eco + (1 | school_ident), data=pisa_ss_na)
paste0("BIC = ",BIC(step.model))
paste0("AIC = ",AIC(step.model))
```



Les AIC et BIC baissent par rapport au modèle linéaire simple. Le modèle à effets mixtes semble mieux ajusté aux données.

On étudie la normalité des résidus du modèle par stepwise: 
```{r model stepwise residus}
residus<-residuals(step.model)
qqnorm(((residus-mean(residus))/sigma(step.model)),main="Modele lmer par stepwise : QQplot des résidus")
qqline(rnorm(100,0,1), col="red")
shapiro.test(residus)
ks.test(residus,pnorm, mean=mean(residus),sd=sigma(step.model))
```

On étudie la normalité des effets aléatoires du modèle par stepwise:

```{r model stepwise ranef}
ranef<-ranef(step.model)
ranef<-ranef[[1]]
ranef<-ranef[,1]
qqnorm(((ranef-mean(ranef))/sd(ranef)),main="Modele lmer par expertise : QQplot des effets aléatoires")
qqline(rnorm(100,0,1), col="red")
shapiro.test(ranef)
ks.test(ranef,pnorm, mean=mean(ranef),sd=sd(ranef))
```