setwd(dir = "P:/projet_stat/ouliers_mm_exploration")
library(tidyverse)
library(magrittr)
library(dplyr)
install.packages("haven")
install.packages("corrplot")
library(corrplot)
install.packages("Hmisc")
library(Hmisc)


pisa <- readRDS("pisa/data/lyceensGT.rds") #sans pondération
pisa_ss_na<-na.omit(pisa)
library(MASS)
# Fit the full model
pisa_ss_ident_school<-pisa_ss_na[,2:26]
full.model <- lm(pisa_ss_na$students_score_lecture ~., data = pisa_ss_ident_school)
model0<-lm(pisa_ss_na$students_score_lecture~1, data=pisa_ss_ident_school)
# Stepwise regression model
step.model <- stepAIC(full.model, direction = "both", 
                      trace = FALSE)
step.model$call
lm(step.model$call)
model_step_lm<-lm(pisa_ss_na$students_score_lecture ~ student_level + 
  student_desk_for_work + student_aimed_diploma + student_repetition + 
  student_outhours + household_index_socio_eco + student_weight + 
  student_litt_at_home + students_score_tr + school_estim_stu_disadvanta + 
  school_status, data = pisa_ss_na)
BIC(model_step_lm)

lm(step.model$call)
model_step_lmer<-lmer(pisa_ss_na$students_score_lecture ~ student_level + 
                    student_desk_for_work + student_aimed_diploma + student_repetition + 
                    student_outhours + household_index_socio_eco + student_weight + 
                    student_litt_at_home + students_score_tr + school_estim_stu_disadvanta + 
                    school_status + (1|school_ident), data = pisa_ss_na)
BIC(model_step_lmer)

notre_model_lm<-lm(students_score_lecture~ 1 + household_index_socio_eco + school_status + student_gender,data = pisa_ss_na)
residuals(notre_model_lmer)

library(lme4)
notre_model_lmer<-lmer(pisa_ss_na$students_score_lecture~ 1 + household_index_socio_eco + school_status + student_gender + (1|school_ident),data = pisa_ss_na)
BIC(notre_model_lmer)
stepwise(full.model, 
         direction = c("backward/forward", "forward/backward", "backward", "forward"), 
         criterion = c("BIC", "AIC"))
ranef<-ranef(notre_model_lmer)
r<-ranef[[1]]
ranef<-r[,1]

mean(ranef)
res.step = step(model0, scope=formula(full.model), direction="both")
res.step$call$formula
residus<-res.step$residuals
shapiro.test(res.step$residuals)
qqnorm(((residus-mean(residus))/sd(residus)))
qqline(rnorm(100,0,1), col="red")

fitted<-res.step$fitted.values
shapiro.test(fitted)
qqnorm(((fitted-mean(fitted))/sd(fitted)))
qqline(((fitted-mean(fitted))/sd(fitted)), col="blue")
qqline(rnorm(100,0,1), col="red")

library(stats)
all<-step(full.model,pisa_ss_na, scale = 0,
     direction = c("both", "backward", "forward"),
     trace = 1, keep = NULL, steps = 1000, k = 2)
all$call

back<-step(full.model,pisa_ss_na, scale = 0,
          direction = "backward",
          trace = 1, keep = NULL, steps = 1000, k = 2)

forward<-step(full.model,pisa_ss_na, scale = 0,
           direction = "forward",
           trace = 1, keep = NULL, steps = 1000, k = 2)



