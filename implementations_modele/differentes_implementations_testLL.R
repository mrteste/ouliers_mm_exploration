
# Comparaison de différentes implémentations du modèle alternatif du test de LL
chemin_repertoire <- "C:/Users/mrteste/Documents/ProjetStat/Outliers_MM_Exploration"
setwd(chemin_repertoire)
source(file = "func_random_intercept.R", encoding = "UTF8")

suppressPackageStartupMessages(library(tidyverse))
suppressPackageStartupMessages(library(lme4))

set.seed(1234)
params <- set_params(h=10, nh=100, h_out = 1, p = 0, tau0 = 1, gammas = 1, U0_out = 5, sig = 1)
data_gen <- gen_Y(params)
str(data_gen)
graph_boxY(data_gen)
f_essaiImplLL <- function(data, num_groupe, ML = TRUE){
  data$In<- ifelse(data$groupe == num_groupe, 0, 1)
  data$I<- ifelse(data$groupe == num_groupe, 1, 0)
  model0 <- lmer(Y~1 + (1|groupe), data = data, REML = !ML)
  model1a <- lmer(Y~1 + I + (1 + I | groupe), data = data, REML = !ML)
  model1b <- lmer(Y~1 + I + (-1 + In | groupe), data = data, REML = !ML)
  model1c <- lmer(Y~1 + In + (-1 + In | groupe), data = data, REML = !ML)
  model1d <- lmer(Y~1 + I + (1 | groupe), data = data, REML = !ML)
  lmodels <- list(model0, model1a, model1b,model1c,model1d)
  names(lmodels) <- c("M0", paste0("M1", letters[1:4]))
  stat_TestLL = as.numeric(map(lmodels, function(x) -2*(logLik(model0) - logLik(x))))
  return(
    list(
      resume = data.frame(
        modele = names(lmodels),
        moy_emp_y = rep(mean(data$Y), 5),
        moy_emp_y_gk = rep(mean(data$Y[data$I==1]), 5),
        moy_emp_y_gnk = rep(mean(data$Y[data$I==0]), 5),
        eff_alea_suspect = as.numeric(
          map(lmodels, function(x) sum(ranef(x)$groupe[num_groupe,]))
        ),
        eff_alea_sum_all = as.numeric(
          map(lmodels, function(x)sum(ranef(x)$groupe))
        ),
        eff_alea_sum_nosuspect = as.numeric(
          map(lmodels, function(x)sum(ranef(x)$groupe[-num_groupe,]))
        ),
        eff_fix_intercept = as.numeric(
          map(lmodels, function(x) fixef(x)[1])
        ),
        eff_fix_indicatrice = as.numeric(
          map(lmodels, function(x) ifelse(length(fixef(x)) == 2, fixef(x)[2], NA))
        ),
        deviance_M0_M1 = stat_TestLL,
        testLL_pvaleur_df1 = 1-pchisq(stat_TestLL, df = 1)
      ),
      modeles = lmodels
    )
  )
}

f_essaiImplLL(data_gen, 10, ML = TRUE)[[1]] %>% gather("variables", "valeurs", -modele) %>% spread(modele, valeurs) %>% knitr::kable(format="latex")
f_essaiImplLL(data_gen, 3, ML = TRUE) %>% gather("variables", "valeurs", -modele) %>% spread(modele, valeurs)
f_essaiImplLL(data_gen, 10, ML = FALSE) %>% gather("variables", "valeurs", -modele) %>% spread(modele, valeurs)
f_essaiImplLL(data_gen, 3, ML = FALSE) %>% gather("variables", "valeurs", -modele) %>% spread(modele, valeurs)

res1a <- f_essaiImplLL(data_gen, 10, ML = TRUE)[["modeles"]][["M1a"]]
res1a
summary(res1a)
ranef(res1a)

res1b <- f_essaiImplLL(data_gen, 10, ML = TRUE)[["modeles"]][["M1b"]]
res1b
summary(res1b)
ranef(res1b)


res1c <- f_essaiImplLL(data_gen, 10, ML = TRUE)[["modeles"]][["M1c"]]
res1c
summary(res1c)
ranef(res1c)

