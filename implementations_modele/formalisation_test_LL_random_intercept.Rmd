---
title: "Formalisation du test de Langford et Lewis"
author: "Nicolas Birot, Olivier Criquet et Julien Jamme"
date: "03/03/2020"
output: 
  pdf_document:
    toc: true
    toc_depth: 2
    number_sections: true
    fig_caption: true
    df_print: kable
    highlight: pygments
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


Le présent document est une tentative d'écrire formellement le test de Langford et Lewis dans le cadre du modèle mixte à effet aléatoire sur l'ordonnée à l'origine - ou *random intercept mixed effect model*.

Nous nous appuyons notamment sur l'ouvrage de José C. Pinheiro et Douglas M.Bates *Mixed-Effects Models in S and S-PLUS*, Springer-verlag, New-York, 2000.  


# Calcul de la vraisemblance du modèle sous l'hypothèse nulle


## Formalisme du modèle 

Dans le cadre du modèle radom intercept et en utilisant le formalisme de Pinheiro et Bates (p. 72 et suivantes) le modèle sous $H_0$ s'écrit sous la forme suivante :  

\begin{equation}
M_0: y_{j} = X_j \beta + Z_j u_{j} + \epsilon_{j}
\end{equation}

Pour nous rapprocher de la formalisation de 

où :  

- $j \in \{1 ... M\}$ indique le numéro du groupe auquel appartient l'individu,
- $y_j$ est un vecteur de taille $n_j$,
- $X_j$ est la matrice des variables explicatives des effets fixes de taille $n_j \times (p+1)$, avec p le nombre de covariables. Dans notre cas, p est nul et $X_j = 1_{n_j}$, c'est-à-dire un vecteur colonne formé de 1,  
- $\beta$ est le vecteur de paramètres des effets fixes, vecteur de taille $p+1$. Dans notre cas, p étant nul, $\beta$ est un réel,  
- $Z_j$ est la matrice des variables explicatives des effets aléatoires de taille $n_j \times (q+1)$, avec q le nombre de covariables. Dans notre cas, q est nul et $Z_j = 1_{n_j}$, c'est-à-dire un vecteur colonne formé de 1,  
- $u_{j}$ vecteur des effets aléatoires associés au groupe j pour chacune des covariables. Ici, q étant nul, $u_j$ est un réel,  
- $\epsilon_j$ est le terme d'erreur résiduelle.

Les hypothèses du modèle sont les suivantes :

- $u_j$ et $\epsilon_j$ sont indépendants,
- $u_j \sim^{iid} \mathcal{N}(0,\tau_0^2)$,
- Les $\epsilon_j \sim^{iid} \mathcal{N}(0,\sigma^2)$.


Ainsi, dans le cadre du modèle random intercept :

\begin{equation}
M_0: y_{j} = 1_{n_j} \beta + 1_{n_j} u_{j} + \epsilon_{j}
\end{equation}

Les auteurs introduisent un facteur de précision relative - "relative precision factor" - $\Delta$ qui, dans notre contexte, est le rapport des écarts-types des effets aléatoires individuels et des effets groupes :  $\Delta = \frac{\sigma}{\tau0}$.  


## Vraisemblance

Par indépendance des observations, la vraisemblance des données est égale au produit des densités des variables aléatoires $Y_j$ de chacun des groupes :

\begin{align*}
L(\beta,\tau_0,\sigma, y) &= f_Y(y_{11},...,y_{n_11},...,y_{1j},...,y_{n_jj},...,y_{1M},...,y_{n_MM}) \\
&=  f_Y(y_1,...,y_j,...,y_M) \\
&= \prod_{j=1}^{M}{f_{Y_j}(y_j)}
\end{align*}

Or, la densité de $Y_j$ n'est pas connue directement, mais en tant que marginale de la densité jointe de $(Y_j, U)$. Ainsi :

$$
L(\beta,\tau_0,\sigma, y) = \prod_{j=1}^{M}\int{f_{Y_j|U=u_j}(y_j)}f_U(uj)du_j
$$

D'après les hypothèses de normalité, on a :

$$
Y_j|U=u_j \sim \mathcal{N}(1_{n_j}\beta + 1_{n_j}u_j, \tau_0^2\mathbf{I}_{n_j})\\
et\\
U \sim \mathcal{N}(0, \sigma^2)
$$
Donc :

$$
f_{Y_j|U=u_j}(y_j) = \frac{\exp(-\frac{1}{2\sigma^2}||y_j - 1_{n_j}\beta - 1_{n_j}u_j||^2)}{(2\pi\sigma^2)^{n_j/2}} \\
et\\
f_U(uj) = \frac{\exp(-\frac{1}{2\tau_0^2}||u_j||^2)}{(2\pi\tau_0^2)^{1/2}} = \frac{\exp(-\frac{1}{2\sigma^2}||\Delta \dot{} u_j||^2)}{(2\pi\sigma^2)^{1/2}\Delta}
$$

Application random intercept

$$
L(\beta , \theta, \sigma^2 / y)
= \prod_{j=1}^m \frac{\sqrt{\frac{\sigma^2}{\tau_0^2}}}{(2 \pi \sigma^2)^{n_{j/2}}}
    \int \frac{exp[-(|| y_j -  1_{n_j} \gamma_0 -  1_{n_j} u_j ||^2 + || \sqrt{\frac{\sigma^2}{\tau_0^2}} - u_j ||^2) / 2 \sigma^2 ]}
    {(2 \pi \sigma^2)^{\frac{1}{2}}} du_j
$$

Application random intercept

$$
\tilde{y}_j =
\begin{bmatrix}
    y_j \\
    0
\end{bmatrix}
\qquad
\tilde{X}_j =
\begin{bmatrix}
    X_j \\
    0
\end{bmatrix}
=
\begin{bmatrix}
    1 \\
    \vdots \\
    1 \\
    0
\end{bmatrix}
\qquad
\tilde{Z}_j =
\begin{bmatrix}
    Z_j \\
    \Delta
\end{bmatrix}
=
\begin{bmatrix}
    1 \\
    \vdots \\
    1 \\
    \sqrt{\frac{\sigma^2}{\tau_0^2}}
\end{bmatrix}
$$

Application random intercept


\begin{align*}
L(\beta , \theta, \sigma^2 / y)
&= \prod_{j=1}^m \frac{\sqrt{\frac{\sigma^2}{\tau_0^2}}}{(2 \pi \sigma^2)^{n_{j/2}}}
    \int \frac{exp[-(|| \tilde{y}_j -  \tilde{X}_j \beta - \tilde{Z}_j u_j ||^2 + || \sqrt{\frac{\sigma^2}{\tau_0^2}} - u_j ||^2) / 2 \sigma^2 ]}
    {(2 \pi \sigma^2)^{\frac{1}{2}}} du_j \\
&= \prod_{j=1}^m \frac{\sqrt{\frac{\sigma^2}{\tau_0^2}}}{(2 \pi \sigma^2)^{n_{j/2}}}
    \int \frac{exp [ - ||
    \begin{pmatrix}
    y_j \\
    0 
    \end{pmatrix}
    -
    \begin{pmatrix}
    1 \\
    \vdots \\
    1 \\
    0 
    \end{pmatrix} \gamma_0
    -
    \begin{pmatrix}
    1 \\
    \vdots \\
    1 \\
    \sqrt{\frac{\sigma^2}{\tau_0^2}}
    \end{pmatrix} u_j ||^2 / 2 \sigma^2 ]
    }
    {(2 \pi \sigma^2)^{\frac{1}{2}}} du_j \\
&= \prod_{j=1}^m \frac{\sqrt{\frac{\sigma^2}{\tau_0^2}}}{(2 \pi \sigma^2)^{n_{j/2}}}
    \int \frac{exp - ||
    \begin{pmatrix}
    y_{1j} - \gamma_0 - u_j \\
    \vdots \\
    \gamma_{n_{j}j} - \gamma_0 - u_j \\ - \sqrt{\frac{\sigma^2}{\tau_0^2}} u_j
    \end{pmatrix}
    ||^2 / 2 \sigma^2
    }
    {(2 \pi \sigma^2)^{\frac{1}{2}}} du_j \\
&= \prod_{j=1}^m \frac{\sqrt{\frac{\sigma^2}{\tau_0^2}}}{(2 \pi \sigma^2)^{n_{j/2}}}
    \int \frac{exp( \frac{-1}{2 \sigma^2} ( \sum_{i=1}^{n_j} (\gamma_{ij} - \gamma_0 - u_j)^2 + \frac{\sigma^2}{\tau_0^2} u_j^2))}
    {(2 \pi \sigma^2)^{\frac{1}{2}}} du_j
\end{align*}


Application au modèle random intercept M1d, avec k le groupe suspecté - sur lequel on applique le test de Langford et Lewis.


$$
Y_{ij} = \gamma_0 + \eta_k \mathbb{1}_{j=k} + u_j + R_{ij}
$$

$$
\left \{
   \begin{array}{l c l}
      y_j  & = & 
        \begin{pmatrix}
            1 & 0 \\
            \vdots & \vdots \\
            1 & 0
        \end{pmatrix} \beta +
        \begin{pmatrix}
            1 \\
            \vdots \\
            1
        \end{pmatrix} u_j + \epsilon_j
        \text{ si } j \ne k \qquad \beta \in \mathbb{R}^{2*1}
      \\
      y_k   & = &
      \begin{pmatrix}
            1 & 1 \\
            \vdots & \vdots \\
            1 & 1
        \end{pmatrix} \beta +
        \begin{pmatrix}
            0 \\
            \vdots \\
            0
        \end{pmatrix} u_k + \epsilon_k \qquad y_{ik} \sim \mathbb{N} (
            \begin{pmatrix}
                \gamma_0 + \eta_k \\
                \vdots \\
                \gamma_0 + \eta_k
            \end{pmatrix}, \sigma^2 I)
   \end{array}
\right.
$$

Par convenance, on choisit k = M (dernier groupe)


\begin{align*}
L(\beta , \theta, \sigma^2 / y)
&= \prod_{j=1}^m \frac{\sqrt{\frac{\sigma^2}{\tau_0^2}}}{(2 \pi \sigma^2)^{n_{j/2}}}
    \int \frac{exp[-(|| \tilde{y}_j -  \tilde{X}_j \beta - \tilde{Z}_j u_j ||^2 + || \sqrt{\frac{\sigma^2}{\tau_0^2}} - u_j ||^2) / 2 \sigma^2 ]}
    {(2 \pi \sigma^2)^{\frac{1}{2}}} du_j \times
    \frac{1}{(2 \pi \sigma^2)^{n_{\frac{M}{2}}}}
    exp(-\frac{1}{2} ( y_k - 
    \begin{pmatrix}
            \gamma_0 + \eta_k \\
            \vdots \\
            \gamma_0 + \eta_k
    \end{pmatrix} )^T
    (\sigma^2 I)^{-1} (y_k -
    \begin{pmatrix}
            \gamma_0 + \eta_k \\
            \vdots \\
            \gamma_0 + \eta_k
    \end{pmatrix} ))\\
&= A_{M-1} \times \frac{1}{(2 \pi \sigma^2)^{n_{\frac{M}{2}}}} exp( -\frac{1}{2 \sigma^2} ||y_M - \begin{pmatrix}
            \gamma_0 + \eta_k \\
            \vdots \\
            \gamma_0 + \eta_k
    \end{pmatrix} ||^2)
\end{align*}

Application au rapport de vraisemblance et au test de la déviance :

$$
\text{H0 : } M_0 : y_{ij} = \gamma_0 + u_j + \epsilon_{ij}
\text{ vs H1 : } M_1 : y_{ij} = \gamma_0 + \eta_k 1_{j=k} + u_j + \epsilon_{ij}
$$

Vraisemblance sous H0 :

$$
\mathcal{L}_0 =
\prod_{j=1}^m \frac{\sqrt{\frac{\sigma^2}{\tau_0^2}}}{(2 \pi \sigma^2)^{n_{j/2}}}
    \int \frac{exp[-(|| \tilde{y}_j -  \tilde{X}_j \beta - \tilde{Z}_j u_j ||^2 + || \sqrt{\frac{\sigma^2}{\tau_0^2}} - u_j ||^2) / 2 \sigma^2 ]}
    {(2 \pi \sigma^2)^{\frac{1}{2}}} du_j
$$

Vraisemblance sous H1 :

$$
\mathcal{L}_1 =
\prod_{j=1, j \ne k}^m \frac{\sqrt{\frac{\sigma^2}{\tau_0^2}}}{(2 \pi \sigma^2)^{n_{j/2}}}
    \int \frac{exp[-(|| \tilde{y}_j -  \tilde{X}_j \beta - \tilde{Z}_j u_j ||^2 + || \sqrt{\frac{\sigma^2}{\tau_0^2}} - u_j ||^2) / 2 \sigma^2 ]}
    {(2 \pi \sigma^2)^{\frac{1}{2}}} du_j
* \frac{1}{(2 \pi \sigma^2)^{n_{\frac{k}{2}}}}
    exp ( - \frac{1}{2 \sigma^2} ||y_k -
    \begin{pmatrix}
            \gamma_0 + \eta_k \\
            \vdots \\
            \gamma_0 + \eta_k
    \end{pmatrix} ||^2)
$$

Statistique de test :


\begin{align*}
T &= -2 ( \text{ln } \mathcal{L}_0 - \text{ln } \mathcal{L}_1) \\
&= -2 \text{ln } \frac{\mathcal{L}_0}{\mathcal{L}_1}
\end{align*}

Avec :

\begin{align*}
\frac{\mathcal{L}_0}{\mathcal{L}_1}
&= \prod_{j=1}^m \frac{\sqrt{\frac{\sigma^2}{\tau_0^2}}}{(2 \pi \sigma^2)^{n_{j/2}}}
    \int \frac{exp[-(|| \tilde{y}_j -  \tilde{X}_j \beta - \tilde{Z}_j u_j ||^2 + || \sqrt{\frac{\sigma^2}{\tau_0^2}} - u_j ||^2) / 2 \sigma^2 ]}
    {(2 \pi \sigma^2)^{\frac{1}{2}}} du_k * (2 \pi \sigma^2)^{n_{\frac{k}{2}}} \times \frac{1}{exp ( - \frac{1}{2 \sigma^2} ||y_k -  \begin{pmatrix}
            \gamma_0 + \eta_k \\
            \vdots \\
            \gamma_0 + \eta_k
    \end{pmatrix} ||^2)} \\
&= \sqrt{\frac{\sigma^2}{\tau_0^2}}
   \int \frac{1}{(2 \pi \sigma^2)^{\frac{1}{2}}}
    exp( - \frac{1}{2 \sigma^2} ||
      \begin{pmatrix}
            y_{1k} - \gamma_0 - u_k \\
            \vdots \\
            y_{n_{k}k} - \gamma_0 - u_k - \sqrt{\frac{\sigma^2}{\tau_0^2}} u_k
      \end{pmatrix} ||^2) du_k    \times exp (\frac{1}{2 \sigma^2} \sum_{i=1}^{n_k} (y_{ik} - \gamma_0 - \eta_k)^2) \\
&= \sqrt{\frac{\sigma^2}{\tau_0^2}}
   \int \frac{1}{(2 \pi \sigma^2)^{\frac{1}{2}}}
    exp( - \frac{1}{2 \sigma^2} [
      \sum_{i=1}^{n_k} (y_{ik} - \gamma_0 - u_k)^2 + \frac{\sigma^2}{\tau_0^2} u_k] du_k
    \times exp (\frac{1}{2 \sigma^2} \sum_{i=1}^{n_k} (y_{ik} - \gamma_0 - \eta_k)^2) \\
&= \sqrt{\frac{\sigma^2}{\tau_0^2}}
   \int \frac{1}{(2 \pi \sigma^2)^{\frac{1}{2}}}
    exp( - \frac{1}{2 \sigma^2} [
      \sum_{i=1}^{n_k} ((y_{ik} - \gamma_0 - u_k)^2 - (y_{ik} - \gamma_0 - \eta_k)^2 ) + \frac{\sigma^2}{\tau_0^2} u_k] du_k \\
&= \sqrt{\frac{\sigma^2}{\tau_0^2}}
   \int \frac{1}{(2 \pi \sigma^2)^{\frac{1}{2}}}
    exp( - \frac{1}{2 \sigma^2} [
      \sum_{i=1}^{n_k} (-2(y_{ik} - \gamma_0)(u_k - \eta_k) + u_k^2 - \eta_k^2 ) + \frac{\sigma^2}{\tau_0^2} u_k] du_k \\
&= \sqrt{\frac{\sigma^2}{\tau_0^2}}
   \int \frac{1}{(2 \pi \sigma^2)^{\frac{1}{2}}}
    exp( - \frac{1}{2 \sigma^2} [
      \sum_{i=1}^{n_k} (-2(u_k - \eta_k) \sum_{i=1}^{n_k} (y_{ik} - \gamma_0) + n_k (u_k^2 - \eta_k^2) u_k^2 - \eta_k^2 ) + \frac{\sigma^2}{\tau_0^2} u_k] du_k \\
&= \sqrt{\frac{\sigma^2}{\tau_0^2}}
   \int \frac{1}{(2 \pi \sigma^2)^{\frac{1}{2}}}
    exp( - \frac{1}{2 \sigma^2} [
      \sum_{i=1}^{n_k} (-2 n_k (u_k - \eta_k)(\overline{y}_k - \gamma_0) + n_k (u_k^2 - \eta_k^2) + \frac{\sigma^2}{\tau_0^2} u_k)] du_k \\
\end{align*}


En posant $\Delta = \frac{\sigma}{\tau0}$, comme précédemment :

$$
\frac{\mathcal{L}_0}{\mathcal{L}_1}
= \Delta
   \int \frac{1}{(2 \pi \sigma^2)^{\frac{1}{2}}} exp( - \frac{1}{2 \sigma^2} [ \sum_{i=1}^{n_k} (-2 n_k (u_k - \eta_k)(\overline{y}_k - \gamma_0) + n_k (u_k^2 - \eta_k^2) + \Delta^2 u_k)] du_k 
$$
