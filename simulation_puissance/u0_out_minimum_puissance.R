packages <- c("dplyr","tidyr","purrr","tibble","forcats",
              "ggplot2","magrittr","lme4","parallel","influence.ME")
p<-lapply(packages, function(x) suppressPackageStartupMessages(library(x, character.only = TRUE)))
source(file = "func_random_intercept.R", encoding = "UTF8")

u0min_rejetLL <- function(data, params){
  require(lme4)
  model0 <- lmer(Y~1 + (1|groupe), data = data, REML = FALSE)
  model1 <- lmer(Y~1 + I + (1|groupe), data = data, REML = FALSE)
  statLL <- as.numeric((-2*(logLik(model0) - logLik(model1))))
  # print(statLL)
  rejetH0 <-  statLL > qchisq(0.95, 1)
  while(!rejetH0){
    data$U0[data$I == TRUE] <- data$U0[data$I == TRUE] + 0.1
    data$Y = params$gammas + data$U0 + data$R
    model0 <- lmer(Y~1 + (1|groupe), data = data, REML = FALSE)
    model1 <- lmer(Y~1 + I + (1|groupe), data = data, REML = FALSE)
    statLL <- as.numeric((-2*(logLik(model0) - logLik(model1))))
    # print(statLL)
    rejetH0 <-  statLL > qchisq(0.95, 1)
  }
  return(unique(data$U0[data$groupe == params$h]))
}

# params <- set_params(h=10, nh=30, h_out = 1, p = 0, tau0 = 3, gammas = 10, U0_out = 0.1, sig = 1)
# set.seed(1234)
# data_gen <- gen_Y(params)
# data_gen$I <- data_gen$groupe == 10
# u0min_rejetLL(data_gen, params)


simul_u0min <- function(params, N){
  foo <- function(params, seed){
    params <- set_params(h=10, nh=30, h_out = 1, p = 0, tau0 = 3, gammas = 10, U0_out = 0.1, sig = 1)
    data_gen <- gen_Y(params)
    data_gen$I <- data_gen$groupe == params$h
    return(u0min_rejetLL(data_gen, params))
  }
  replicate(
    N,
    foo(params)
  )
}

# params <- set_params(h=10, nh=30, h_out = 1, p = 0, tau0 = 3, gammas = 10, U0_out = 0.1, sig = 1)
# simul_u0min(params, 10)

nb_simulations <- 10000
no_cores <- detectCores() - 1
cat("nombre de coeurs sollicités", no_cores)
nbsimbycores <- c(rep(trunc(nb_simulations/no_cores),no_cores-1),trunc(nb_simulations/no_cores)+nb_simulations%%no_cores)


#Avec 10 groupes
start <- proc.time()
cl <- makeCluster(no_cores)
clusterExport(
  cl,
  c("simul_u0min", "u0min_rejetLL", "set_params", "gen_Y",
    "gen_expl", "gen_alea", "nb_simulations",
    "nbsimbycores", "res_simul")
)
print("simul")
res_simul <- parSapply(cl, nbsimbycores, function(x){
  params <- set_params(h=10, nh=30, h_out = 1, p = 0, tau0 = 3, gammas = 10, U0_out = 0.1, sig = 1)
  return(simul_u0min(params, x))
})
stopCluster(cl)
ecoule <- proc.time() - start

moy <- mean(unlist(res_simul)) #6.008
moy - 1.96*sd(unlist(res_simul))/sqrt(nb_simulations) #5.97
moy + 1.96*sd(unlist(res_simul))/sqrt(nb_simulations) #6.04

plot(unlist(res_simul))


#avec 100 groupes
start <- proc.time()
cl <- makeCluster(no_cores)
clusterExport(
  cl,
  c("simul_u0min", "u0min_rejetLL", "set_params", "gen_Y",
    "gen_expl", "gen_alea", "nb_simulations",
    "nbsimbycores", "res_simul")
)
print("simul")
res_simul_100gr <- parSapply(cl, nbsimbycores, function(x){
  params <- set_params(h=100, nh=30, h_out = 1, p = 0, tau0 = 3, gammas = 10, U0_out = 0.1, sig = 1)
  return(simul_u0min(params, x))
})
stopCluster(cl)
ecoule <- proc.time() - start

moy <- mean(unlist(res_simul_100gr)) #5.96731
moy - 1.96*sd(unlist(res_simul_100gr))/sqrt(nb_simulations) #5.931407
moy + 1.96*sd(unlist(res_simul_100gr))/sqrt(nb_simulations) #6.003213


#avec 100 groupes et tau0 = sigma
start <- proc.time()
cl <- makeCluster(no_cores)
clusterExport(
  cl,
  c("simul_u0min", "u0min_rejetLL", "set_params", "gen_Y",
    "gen_expl", "gen_alea", "nb_simulations",
    "nbsimbycores", "res_simul")
)
print("simul")
res_simul_100gr_b <- parSapply(cl, nbsimbycores, function(x){
  params <- set_params(h=100, nh=30, h_out = 1, p = 0, tau0 = 1, gammas = 10, U0_out = 0.1, sig = 1)
  return(simul_u0min(params, x))
})
stopCluster(cl)
ecoule <- proc.time() - start

moy <- mean(unlist(res_simul_100gr_b)) #
moy - 1.96*sd(unlist(res_simul_100gr_b))/sqrt(nb_simulations) #
moy + 1.96*sd(unlist(res_simul_100gr_b))/sqrt(nb_simulations) #


